

export class Room{

    constructor(number,rm_type,terrace){
        this.number=number;
        this.rm_type=rm_type;
        this.terrace=terrace;
    }

    getNumber(){
        return this.number;
    }

    getRm_Type(){
        return this.rm_type;
    }

    getTerrace(){
        return this.terrace;
    }
}