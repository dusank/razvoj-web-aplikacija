import { interval, range, Subject, Observable, fromEvent, from, merge ,forkJoin, timer, zip } from "rxjs";
import { delay, take, filter, map, takeUntil, sampleTime, mapTo , debounceTime, switchMap, pairwise, scan, flatMap } from "rxjs/operators";
import {Meal} from "./meal"
import {Hotel} from "./hotel"
import { async } from "rxjs/internal/scheduler/async";
import { brotliDecompressSync } from "zlib";



 function getUser() {
        return from(
            fetch("http://localhost:3000/users")
            .then(res=>res.json())
        )
  }


  const buttonLog=document.getElementById("log-in");
function changePage(){
    buttonLog.innerHTML=inpUser.value;
}
const btnLogIn=document.getElementById("log-in-button");

// function logUser(){
//     return merge(
//     fromEvent(inpUser,'input')
//     .pipe(
//         map(ev=>ev.target.value),
//         filter(n=>n.username=="admin"),
//         switchMap(getUser())
//     ),
//     fromEvent(inpPass,"input").
//     pipe(
//         delay(2000),
//         map(ev=>ev.target.value),
//         filter(p=>p.password=="admin"),
//         switchMap(getUser())
//     ))
// }



btnLogIn.onclick=(ev)=>getUser().subscribe(x=>{
    hotel.Users=x;
    console.log(hotel.Users);
    hotel.logUser(ev);
    $('#log-in-form').modal('hide');    
})



function randomId(){
    return Observable.create((generator)=>{
        setInterval(()=>{          
            generator.next(parseInt(Math.random()*10))
        },1000)
        
    })
}



   
function threeRandomMeals(){    
    return randomId().pipe(
        debounceTime(50),
        take(3),    
        switchMap(x=>getMeal(x))
    )
}

threeRandomMeals().subscribe(x=>hotel.addMeal(x));

function promiseAsync(){
    return new Promise(res=>
        setTimeout(()=>{
            res("done");
        },5000)
        )
}
let hotel=new Hotel();
async function asyncWait(){
    console.log("AJDE");
    let res=await promiseAsync();
    console.log(res);
    console.log(hotel.Meals);
    hotel.fillSelect();
    hotel.Meals.forEach(el=>hotel.drawModal(el)); 
    hotel.drawRandom();   
    btnSurprise=document.getElementById("surprise-me");
    btnSurprise.className="btn btn-outline-dark";
    const lblSurprise=document.getElementById("surprise-label");
    const imgSurprise=document.getElementById("surprise-pic");
    const cbSurprise=document.getElementById("surprise-cb");
    btnSurprise.onclick=()=>getRandomMeal().subscribe(x=>{
        lblSurprise.innerHTML=x.name;
        imgSurprise.src=x.picture;
        cbSurprise.setAttribute("id",x.id+"cb-food")
        cbSurprise.value=x.id;
        console.log(x.picture);
    });
    orderBtn=document.getElementById("order-food");
    const lblFood=document.getElementById("lbl-food");
        orderBtn.onclick=()=>{
            hotel.Meals.forEach(ev=>{
            hotel.orderFood(ev);
        })
        alert(lblFood.innerHTML);
    }
    
}
let btnSurprise=null;


asyncWait();

const btnTableReservation=document.getElementById("room-button");


function getRooms(){
    return from(
        fetch("http://localhost:3000/rooms")
        .then(res=>res.json())
    )
}

function randomRoomId(){
    return Observable.create((generator)=>{
        setInterval(()=>{          
            generator.next(parseInt(Math.random()*5))
        },15000)
        
    }).pipe(
        take(1)
    )
}
const lab1=document.getElementById("lab1");
const lab2=document.getElementById("lab2");

let orderBtn=null;
let bookBtn=null;

getRooms().subscribe(x=>{
    hotel.Rooms=x;
    hotel.countRooms();
    bookBtn=document.getElementById("book-it");
    bookBtn.onclick=()=>{
        hotel.bookIt();
        $('#log-in-form').modal('hide');
    };

    setInterval(()=>randomRoomId().subscribe(i=>{
        hotel.deleteElement(i);
        console.log(hotel.Rooms)
        hotel.countRooms(); 
    }),150000)
    
})




function getMeal(id){
    return from(
        fetch(`http://localhost:3000/meals/${id}`)
        .then(res=>res.json())
        )
    }
    

    

    function getRandomMeal(){
        randomId().subscribe(val => btnSurprise.value = val);
        return fromEvent(btnSurprise,'click')
        .pipe(
            debounceTime(50),
            map(ev => ev.target.value),
            filter(id=>id>0),
            switchMap(id=>getMeal(id)),        
        )
    }
     
   
    


