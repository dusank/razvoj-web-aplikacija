

export class Meal{

    constructor(name,type,prep_time,source){
        this.name=name;
        this.type=type;
        this.prep_time=prep_time;
        this.source=source;
    }

    get Name(){
        return this.name;
    }
    
    get Type(){
        return this.type;
    }

    get PrepTime(){
        return this.prep_time;
    }

    get ImgSrc(){
        return this.source;
    }

    getMeal(){
        echo `Vas obrok je ${this.name} koji se sprema za ${this.prep_time}`;
    }
}