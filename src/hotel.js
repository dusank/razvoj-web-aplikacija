import {Meal} from "./meal"
import { create } from "domain";

export class Hotel{

    constructor() {
        this.meals= new Array();
        this.rooms= new Array();
        this.users= new Array();
    }

    addMeal(x){
        this.meals.push(x);
    }

    addRoom(x){
        this.rooms.push(x);
    }

    get Meals(){
        return this.meals;
    }

    set Meals(x){
        this.meals=x;
    }

    get Rooms(){
        return this.rooms;
    }
    
    set Rooms(x){
        this.rooms=x;
    }

    get Users(){
        return this.users;
    }
    
    set Users(x){
        this.users=x;
    }

    bookIt(){
        const lblBook=document.getElementById("lbl-book");
        const radioSelectedBeds=document.querySelector("input[name='room-size']:checked");
        const radioSelectedView=document.querySelector("input[name='room-view']:checked");
            if(radioSelectedBeds.value==2)
                lblBook.innerHTML="Room with 2 beds";
            else
                lblBook.innerHTML="Room with 3 beds";
            if(radioSelectedView.value=="yes"){
                lblBook.innerHTML+=" with beach view";
                alert(`You booked room ${radioSelectedBeds.value} and beach view`);
                // this.rooms.splice(indexOf(`${radioSelectedBeds.value}-bed`),1);
                console.log(this.rooms);
            }
            else{
                lblBook.innerHTML+=" without beach view";
                alert(`You booked room ${radioSelectedBeds.value} and beach view`);
                this.rooms.splice(indexOf(`${radioSelectedBeds.value}-bed`),1);
                console.log(this.rooms);
            }        
    }

    orderFood(ev){
        const cbFood=document.getElementById(`${ev.id}cb-food`);
            const lblFood=document.getElementById("lbl-food");
            if(cbFood.checked){
                if(cbFood.value==ev.id)
                lblFood.innerHTML+=` ${ev.name}`;
            }
        
    }


    changePage(user){
        const userButton=document.getElementById("user-button");
        const logButton=document.getElementById("log-in");
        const lblName=document.getElementById("user-title");
        logButton.hidden=true;
        userButton.innerHTML=user.username;
        userButton.hidden=false;
        lblName.innerHTML=user.username;
    }
    
    logUser(ev){
        const inpUser=document.getElementById("username");
        const inpPass=document.getElementById("password");
        this.users.forEach(el=>{
            if(el.username==inpUser.value && el.password==inpPass.value)
            this.changePage(el);
            else
            alert("Nepostojeci korisnik");
        }
        )
    }

    countRooms(){
        let bed2=0;
        let bed3=0;
        this.rooms.forEach(el=>{
            if(el.rm_type=="2-bed")
                bed2++;
            else
                bed3++;
        })
        lab1.innerHTML=bed2;
        lab2.innerHTML=bed3;
    }

    fillSelect(){
        const divBody=document.getElementById("table-modal-body");
        let niz=["Table for 2","Table for 3","Table for 4","Table for 6"];
        let div=document.createElement("div");
        div.className="selekt-div";
        divBody.appendChild(div);
        let selekt=document.createElement("select");        
        let element=null;
        niz.forEach((el,i)=>{
            element=document.createElement("option");
            element.innerHTML=el;
            element.value=i;
            selekt.appendChild(element);
        })
        div.appendChild(selekt);
    }

    drawModal(meal){
        const divBody=document.getElementById("table-modal-body");
        const div=document.createElement("div");
        div.className="innerModalBody";
        divBody.appendChild(div);
        let el=document.createElement("img");
        el.src=meal.picture;
        div.appendChild(el);
        el=document.createElement("label");
        el.value=meal.id;
        el.innerHTML=meal.name;
        div.appendChild(el);
        el=document.createElement("input");
        el.setAttribute("id",meal.id+"cb-food");
        el.value=meal.id;
        el.setAttribute("type", "checkbox");
        div.appendChild(el);
    }

    
    drawRandom(){
        const divBody=document.getElementById("table-modal-body");
        const div=document.createElement("div");
        div.className="innerModalBody";
        divBody.appendChild(div);
        let el=document.createElement("img");
        el.src="./src/img/question-mark.png";
        el.setAttribute("id","surprise-pic");
        div.appendChild(el);
        el=document.createElement("label");
        el.innerHTML="?";
        el.setAttribute("id","surprise-label");
        div.appendChild(el);
        el=document.createElement("input");
        el.setAttribute("type", "checkbox");
        div.appendChild(el);
        const divBtn=document.createElement("div");
        divBtn.className="divBtnModal";
        divBody.appendChild(divBtn);
        let button=document.createElement("button");
        button.innerHTML="Surprise me";
        button.setAttribute("id", "surprise-me");
        divBtn.appendChild(button);
        button=document.createElement("button");
        button.className="btn btn-outline-dark";
        button.innerHTML="Order";
        button.setAttribute("id", "order-food");
        divBtn.appendChild(button);
    }

    drawRoomModal(){
        const divBody=document.getElementById("room-modal-body");
        let div=document.createElement("div");
        div.className="room-modal-div";
        divBody.appendChild(div);
        let divTwo=document.createElement("div");
        divTwo.className="top-radios"
        div.appendChild(divTwo);
        let span=document.createElement()
        let el=document.createElement("input");
        el.setAttribute("type","radio");
        el.value=2;
        el.innerHTML="2-bed room";
        divTwo.appendChild(el);
        el=document.createElement("input");
        el.setAttribute("type","radio");
        el.value=3;
        el.innerHTML="3-bed room"
        divTwo.appendChild(el);
        el=document.createElement("input");
        el.setAttribute("type","radio");
        el.value="yes";
        el.innerHTML="Ocean view terrace"
        div.appendChild(el);
        el=document.createElement("input");
        el.setAttribute("type","radio");
        el.value="no";
        el.innerHTML="Without a terrace"
        div.appendChild(el);
    }

    deleteElement(el){
        this.rooms.splice(el,1);
    }
}